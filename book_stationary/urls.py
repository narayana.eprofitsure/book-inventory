from django.conf.urls import url
from django.urls import path
from django.conf import settings
from .import views
from django.conf.urls.static import static

urlpatterns = [
	url(r'^$', views.Home, name='view_inven'),
	url(r'^add_books$', views.AddBooks, name='add_book'),
	url(r'^edit_books/(?P<pk>\d+)$', views.EditBook, name='edit_book'),
	url(r'^search_book$', views.SearchBook, name='search_book'),
	url(r'^delete_books/(?P<pk>\d+)$', views.DeleteBook, name='delete_book'),
]