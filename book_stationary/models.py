from django.db import models

# Create your models here.
class BookInventory(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    isbn = models.CharField(max_length=200, blank=True, null=True)
    authors = models.CharField(max_length=200, blank=True, null=True)
    publishers = models.CharField(max_length=200, blank=True, null=True)
    price = models.CharField(max_length=255, blank=True, null=True)
    quantity = models.CharField(max_length=255, blank=True, null=True)

    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Meta."""

        managed = True
        db_table = "book_inventory"
        ordering = ["pk"]

    def __str__(self):
        """__str__."""
        return str(self.isbn) + str(self.title)