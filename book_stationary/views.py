import requests
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import HttpResponse
from .models import BookInventory


def Home(request):
	print('im here')
	book_obj = BookInventory.objects.all()
	print(book_obj, 'obj')
	return render (request, 'index.html', {'data': book_obj})


def get_google_book(title):
	google_book = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + title)
	google_book = google_book.json()
	print(google_book, 'google book output')
	return google_book['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']


@csrf_exempt
def AddBooks(request):
	if request.method == 'POST':
		print(request.POST, 'request data')
		print(request.POST.get('title'), 'data title')
		print(type(request.POST.get('title')), 'type')

		create_book = BookInventory()
		create_book.title = request.POST.get('title')
		try:
			create_book.isbn = get_google_book(request.POST.get('title'))
		except Exception as e:
			print(e, 'exception')
		create_book.authors = request.POST.get('author')
		create_book.publishers = request.POST.get('publisher')
		create_book.price = request.POST.get('price')
		create_book.quantity = request.POST.get('quantity')
		create_book.save()
		book_obj = BookInventory.objects.all()
		return render (request, 'index.html', {'data': book_obj})
	print('im in get')
	return render (request, 'add_books.html')


@csrf_exempt
def EditBook(request, pk):
	book_obj = BookInventory.objects.filter(id=pk).first()

	if request.method == 'POST':
		book_obj.title = request.POST.get('title')
		book_obj.authors = request.POST.get('author')
		book_obj.publishers = request.POST.get('publisher')
		book_obj.price = request.POST.get('price')
		book_obj.quantity = request.POST.get('quantity')
		book_obj.save()
		book_obj = BookInventory.objects.all()
		return render (request, 'index.html', {'data': book_obj})

	
	print(book_obj, 'obj')
	return render (request, 'update_inventory.html', {'data': book_obj})


def DeleteBook(request, pk):
	print(pk, 'pk')
	book_obj = BookInventory.objects.filter(id=pk).first()
	book_obj.delete()
	book_obj = BookInventory.objects.all()
	return render (request, 'index.html', {'data': book_obj})


@csrf_exempt
def SearchBook(request):
	print('im here')
	if request.method == 'POST':
		print(request.POST.get('isbn'))
		print(type(request.POST.get('title')))
		if request.POST.get('isbn'):
			book_obj = requests.get('https://www.googleapis.com/books/v1/volumes?q=isbn:' + request.POST.get('isbn'))
			google_book = book_obj.json()
			print(google_book, 'gobiall')
		else:
			print('else')
			book_obj = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.POST.get('title'))
			google_book = book_obj.json()
		return render (request, 'search_table.html', { 'data': google_book })
	else:
		google_book = None

	return render (request, 'search.html')
	

	