from django.apps import AppConfig


class BookStationaryConfig(AppConfig):
    name = 'book_stationary'
